var turnoInicial = 'X' ;

$(document).ready(function () {
        IniciarJuego();
	})

	function cargar(tipoJugada) {
		console.log(tipoJugada);
		$(".alert").hide();
		var turno = turnoInicial;
		cambioJugada();
		var posicionX = tipoJugada.attributes.posicionX.value;
		var posicionY = tipoJugada.attributes.posicionY.value;
		var tipo = true;
		
		console.log("Posicion x "+posicionX);
		console.log("Posicion Y "+posicionY);
		console.log("turno "+turno);
		if (turno == 'X'){
            tipo = true;
		}else{
			tipo = false;
		}
		
		console.log("tipo "+tipo);
		
		$.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: 'http://localhost:8080/test/validar_triki?posicionX='
                + posicionX + '&posicionY=' + posicionY + '&type=' + tipo,
            data: "",
			success: function (respuesta) {
				
                switch (respuesta) {
                    case '1':
                        $("#"+tipoJugada.id).html(turno)
                        break;
                    
                    case '2':
                        opcionCambioTurno()
                        break;
                    
                    case '3':
                        validacion(turno);
                        $("#"+tipoJugada.id).html(turno)
                    default:
                        break;
                }
            },
            error: function (x) {
                console.log(x);
            }
        });
	}

	function cambioJugada(){
		if (turnoInicial == 'X'){
			turnoInicial = 'O';
		}else{
			turnoInicial = 'X'   
			}

		}

	function opcionCambioTurno(){
		$(".alert-danger").show();
        cambioJugada()
	}

	function validacion(jugador){
        $(".alert-success").show();
        $(".alert-success").html('El ganador es '+jugador+'!')
        $(".juego").hide();
        
	}
	
	function IniciarJuego(){
        $(".btn-success").html('-')
        $(".juego").show();
        $(".alert").hide();
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: "http://localhost:8080/test/iniciarMatrixTriki",
            success: function (data) {
                console.log(data)
            },
            error: function (x) {
                console.log(x);
            }
        });
    }
	
		
		
	